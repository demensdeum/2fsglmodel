mkdir include
cd include
git clone https://gitlab.com/demensdeum/FSGL/
mkdir FlameSteelFramework
cd FlameSteelFramework
git clone https://gitlab.com/demensdeum/FlameSteelEngineGameToolkit
git clone https://gitlab.com/demensdeum/FlameSteelCore
git clone https://gitlab.com/demensdeum/FlameSteelBattleHorn
git clone https://gitlab.com/demensdeum/FlameSteelCommonTraits
cd ..
rm -rf FSGL/src/FSGL/Renderer/OGLNewAgeRenderer
rm FSGL/src/FSGL/Core/FSGLCore.cpp
rm FSGL/src/FSGL/Controller/FSGLController.cpp
