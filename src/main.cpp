#include <iostream>
#include <fstream>
#include "FSGLModelLoaderAssimp.h"

using namespace std;

int main(int argc, char *argv[]) {

	cout << "2FSGLModel - 3D models to fsgl converter" << endl;

	if (argc != 2) {
		cout << "Usage: 2FSGLModel inputModel.obj" << endl;
		exit(1);
	}

	auto inputModelPath = make_shared<string>(argv[1]);
	auto outputModelPath = string(argv[1]);
    outputModelPath += ".fsglmodel";
	auto loader = shared_ptr<FSGLModelLoaderAssimp>();
	auto model = loader->loadModel(inputModelPath, false);

	auto serialized = model->serializeIntoString(true);

	auto outputFile = ofstream(outputModelPath);
	outputFile << *serialized << endl;
	outputFile.close();
};
