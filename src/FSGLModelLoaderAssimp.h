/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FSModelLoaderObj.h
 * Author: demensdeum
 *
 * Created on July 9, 2017, 10:07 AM
 */

#ifndef FSMODELLOADEROBJ_H
#define FSMODELLOADEROBJ_H

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <FSGL/Data/ModelLoader/FSGLModelLoader.h>

#include <assimp/scene.h>

class FSGLVertex;

#include <memory>

using namespace std;

class FSGLModelLoaderAssimp: public FSGLModelLoader {
public:
    FSGLModelLoaderAssimp();
    virtual ~FSGLModelLoaderAssimp();
    
    static shared_ptr<FSGLModel> loadModel(shared_ptr<string> modelPath, bool shouldLoadTextures = true);
    
private:
    
	static shared_ptr<FSGLMatrix> convertAssimpMatrixToFSGLMatrix(const aiMatrix4x4 assimpMatrix);
    static shared_ptr<FSGLNode> convertNode(aiNode *node, shared_ptr<FSGLNode> parentNode, shared_ptr<FSGLModel> model);
    
};

#endif /* FSMODELLOADEROBJ_H */

